

# Stack

Running on Symfony 7, PHP 8.3, and a docker container with a local postgres DB

# Description

When viewing a short url like http://127.0.0.1:8000/t/YWQ3MDA3M2
If "YWQ3MDA3M2" is saved in the DB, it will redirect to the long_url saved in the DB


## CSV Upload

Can upload URLs to the DB through Insomnia (POST localhost:8000/urls) or the UI (http://127.0.0.1:8000/uploadCsv) 
An example CSV is provided in the codebase (ExampleUrlsData.csv)

## URL Rules

URLs are sanitized to be URL safe characters only. 
HTML chars are encoded upon save to DB
Leading and trailing "/" will be trimmed from url before DB save
No urls can contain "/" in the middle, breaks routing


## Metrics

Metrics can be viewed in Insomnia with the Short URL (localhost:8000/metrics/YWQ3MDA3M2)

Visit_count is the only metric that changes upon page view right now 



# Dev commands 

Spin up docker: 
```docker-compose up --build```

View docker containers
```docker ps```

Debug Routes
```php bin/console debug:router```

Exec into DB
```docker exec -it wemodproject-database-1 psql -U app -h localhost```

Fix Docterine Schema After Col Change
```php bin/console doctrine:schema:update --dump-sql```
```php bin/console doctrine:schema:update --force```
<!-- https://stackoverflow.com/questions/45648139/unable-to-insert-data-into-postgresql-in-symfony-with-complex-relation -->



# Local testing
Spin up docker + symfony: 
```docker-compose up --build```
```symfony server:start```

Should be able to load http://127.0.0.1:8000 this point


## Things that could be improved

Abstract Methods out into seperate controllers 
Unit Tests
Better UI
Better timestamp updating in db - on visit_count update + store timestamps
Exception handling
Edge Case Handling of url string data
Type handling of inputs






